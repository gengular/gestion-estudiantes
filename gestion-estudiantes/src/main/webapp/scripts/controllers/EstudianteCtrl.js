'use strict';

var app = angular.module('GestionEstudiantesApp.Estudiantes', ["ngRoute", "ngMessages"]);

app.controller('EstudianteCtrl', ['$scope', '$routeParams', '$location', '$http', 'BASE',
    function ($scope, $routeParams, $location, $http, BASE) {

        $scope.estudiantes;
        $scope.estudiante;
        $scope.pageSize = 5;
        $scope.currentPage = 1;

        $scope.irEditarEstudiante = function (idEstudiante) {
            $location.path('/editar_estudiante/' + idEstudiante);
        };

        $scope.editarEstudiante = function () {
            if ($scope.editarEstudianteForm.$valid) {
                $http.post(BASE + '/resources/estudiante/actualizarEstudiante', angular.toJson($scope.estudiante))
                        .success(function (data) {
                            sweetAlert("Correcto", "El estudiante ha sido modificado correctamente.", "success");
                            $location.path('/consultar_estudiantes');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible modificar el estudiante, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }
        };

        $scope.irCrearEstudiante = function () {
            $location.path('/crear_estudiante');
        };

        $scope.crearEstudiante = function () {
            if ($scope.crearEstudianteForm.$valid) {
                $http.post(BASE + '/resources/estudiante/agregarEstudiante', angular.toJson($scope.estudiante))
                        .success(function (data) {
                            sweetAlert("Correcto", "El estudiante ha sido creado correctamente.", "success");
                            $location.path('/consultar_estudiantes');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible crear el estudiante, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }
        };

        $scope.eliminarEstudiante = function (idEstudiante) {
            $http.post(BASE + '/resources/estudiante/eliminarEstudiante', angular.toJson(idEstudiante))
                    .success(function (data) {
                        $scope.consultarEstudiantes();
                        sweetAlert("Correcto", "El estudiante ha sido eliminado correctamente.", "success");
                    })
                    .error(function (data) {
                        sweetAlert("Error", "No fue posible eliminar el estudiante, por favor intente de nuevo.", "error");
                    });
        };

        $scope.consultarEstudiantes = function () {
            $http.get(BASE + '/resources/estudiante/obtenerTodosEstudiantes').
                    then(function (response) {
                        $scope.estudiantes = response.data;
                    });
        };

        $scope.consultarEstudiante = function () {
            $http.get(BASE + '/resources/estudiante/obtenerEstudiante/' + $routeParams.id).
                    then(function (response) {
                        $scope.estudiante = response.data;
                    });
        };

        $scope.cancelar = function () {
            $location.path('/consultar_estudiantes');
        };

    }]);

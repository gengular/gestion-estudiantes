'use strict';

var app = angular.module('GestionEstudiantesApp.Asignaturas', ["ngRoute", "ngMessages", "ui.bootstrap"]);

app.controller('AsignaturaCtrl', ['$scope', '$routeParams', '$location', '$http', 'BASE',
    function ($scope, $routeParams, $location, $http, BASE) {

        $scope.asignaturas;
        $scope.asignatura;
        $scope.semestres = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
        $scope.facultades;
        $scope.pageSize = 5;
        $scope.currentPage = 1;

        $scope.irEditarAsignatura = function (idAsignatura) {
            $location.path('/editar_asignatura/' + idAsignatura);
        };

        $scope.editarAsignatura = function () {
            if ($scope.editarAsignaturaForm.$valid) {
                $http.post(BASE + '/resources/asignatura/actualizarAsignatura', angular.toJson($scope.asignatura))
                        .success(function (data) {
                            sweetAlert("Correcto", "La asignatura ha sido modificada correctamente.", "success");
                            $location.path('/consultar_asignaturas');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible modificar la asignatura, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }
        };

        $scope.irCrearAsignatura = function () {
            $location.path('/crear_asignatura');
        };

        $scope.crearAsignatura = function () {
            if ($scope.crearAsignaturaForm.$valid) {
                $http.post(BASE + '/resources/asignatura/agregarAsignatura', angular.toJson($scope.asignatura))
                        .success(function (data) {
                            sweetAlert("Correcto", "La asignatura ha sido creada correctamente.", "success");
                            $location.path('/consultar_asignaturas');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible crear la asignatura, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }

        };

        $scope.eliminarAsignatura = function (idAsignatura) {
            $http.post(BASE + '/resources/asignatura/eliminarAsignatura', angular.toJson(idAsignatura))
                    .success(function (data) {
                        $scope.consultarAsignaturas();
                        sweetAlert("Correcto", "La asignatura ha sido eliminada correctamente.", "success");
                    })
                    .error(function (data) {
                        sweetAlert("Error", "No fue posible eliminar la asignatura, por favor intente de nuevo.", "error");
                    });
        };

        $scope.cancelar = function () {
            $location.path('/consultar_asignaturas');
        };

        $scope.consultarAsignaturas = function () {
            $http.get(BASE + '/resources/asignatura/obtenerTodasAsignaturas').
                    then(function (response) {
                        $scope.asignaturas = response.data;
                    });
        };

        $scope.consultarAsignatura = function () {
            $scope.consultarFacultades();
            $http.get(BASE + '/resources/asignatura/obtenerAsignatura/' + $routeParams.id).
                    then(function (response) {
                        $scope.asignatura = response.data;
                    });
        };

        $scope.consultarFacultades = function () {
            $http.get(BASE + '/resources/facultad/obtenerTodasFacultades').
                    then(function (response) {
                        $scope.facultades = response.data;
                    });
        };

    }]);

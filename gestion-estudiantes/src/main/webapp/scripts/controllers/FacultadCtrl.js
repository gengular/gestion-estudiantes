'use strict';

var app = angular.module('GestionEstudiantesApp.Facultades', ["ngRoute", "ngMessages", "ui.bootstrap"]);

app.controller('FacultadCtrl', ['$scope', '$routeParams', '$location', '$http', 'BASE',
    function ($scope, $routeParams, $location, $http, BASE) {

        $scope.facultades;
        $scope.facultad;
        $scope.pageSize = 5;
        $scope.currentPage = 1;

        $scope.irEditarFacultad = function (idFacultad) {
            $location.path('/editar_facultad/' + idFacultad);
        };

        $scope.editarFacultad = function () {
            if ($scope.editarFacultadForm.$valid) {
                $http.post(BASE + '/resources/facultad/actualizarFacultad', angular.toJson($scope.facultad))
                        .success(function (data) {
                            sweetAlert("Correcto", "La facultad ha sido modificada correctamente.", "success");
                            $location.path('/consultar_facultades');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible modificar la facultad, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }
        };

        $scope.irCrearFacultad = function () {
            $location.path('/crear_facultad');
        };

        $scope.crearFacultad = function () {
            if ($scope.crearFacultadForm.$valid) {
                $http.post(BASE + '/resources/facultad/agregarFacultad', angular.toJson($scope.facultad))
                        .success(function (data) {
                            sweetAlert("Correcto", "La facultad ha sido creada correctamente.", "success");
                            $location.path('/consultar_facultades');
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible crear la facultad, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "La información se encuentra incompleta, por favor verifique.", "error");
            }

        };

        $scope.eliminarFacultad = function (idFacultad) {
            $http.post(BASE + '/resources/facultad/eliminarFacultad', angular.toJson(idFacultad))
                    .success(function (data) {
                        $scope.consultarFacultades();
                        sweetAlert("Correcto", "La facultad ha sido eliminada correctamente.", "success");
                    })
                    .error(function (data) {
                        sweetAlert("Error", "No fue posible eliminar la facultad, por favor intente de nuevo.", "error");
                    });
        };

        $scope.cancelar = function () {
            $location.path('/consultar_facultades');
        };

        $scope.consultarFacultad = function () {
            $http.get(BASE + '/resources/facultad/obtenerFacultad/' + $routeParams.id).
                    then(function (response) {
                        $scope.facultad = response.data;
                    });
        };

        $scope.consultarFacultades = function () {
            $http.get(BASE + '/resources/facultad/obtenerTodasFacultades').
                    then(function (response) {
                        $scope.facultades = response.data;
                    });
        };

    }]);
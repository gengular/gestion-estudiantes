'use strict';

var app = angular.module('GestionEstudiantesApp.Inscripciones', ["ngRoute", "ngMessages", "ui.bootstrap"]);


app.controller('InscripcionCtrl', ['$scope', '$routeParams', '$location', '$http', 'BASE',
    function ($scope, $routeParams, $location, $http, BASE) {

        $scope.numeroDocumento;
        $scope.estudiante;
        $scope.asignaturas;
        $scope.asignaturasPorInscribir;
        $scope.asignaturaSeleccionada;
        $scope.mostrarPanel = false;
        $scope.pageSize = 5;
        $scope.currentPage = 1;

        $scope.buscarEstudiante = function () {
            if ($scope.gestionarInscripcionForm.$valid) {
                $http.get(BASE + '/resources/inscripcion/obtenerEstudiante/' + $scope.numeroDocumento).
                        then(function (response) {
                            $scope.estudiante = response.data;
                            $scope.consultarAsignaturasInscritas();
                            $scope.consultarAsignaturasPorInscribir();
                        });
                $scope.mostrarPanel = true;
            } else {
                sweetAlert("Error", "El número de documento del estudiante es obligatorio.", "error");
                $scope.mostrarPanel = false;
            }

        };

        $scope.consultarAsignaturasInscritas = function () {
            $http.get(BASE + '/resources/inscripcion/obtenerAsignaturasInscritas/' + $scope.estudiante.id).
                    then(function (response) {
                        $scope.asignaturas = response.data;
                    });
        };

        $scope.inscribirAsignatura = function () {
            if ($scope.asignaturaSeleccionada) {
                var inscripcion = {idEstudiante: $scope.estudiante.id, idAsignatura: $scope.asignaturaSeleccionada.id};
                console.log(inscripcion);
                $http.post(BASE + '/resources/inscripcion/inscribirAsignatura', inscripcion)
                        .success(function (data) {
                            $scope.consultarAsignaturasInscritas();
                            $scope.consultarAsignaturasPorInscribir();
                            sweetAlert("Correcto", "Asignatura inscrita con éxito.", "success");
                        })
                        .error(function (data) {
                            sweetAlert("Error", "No fue posible inscribir la asignatura, por favor intente de nuevo.", "error");
                        });
            } else {
                sweetAlert("Error", "Debe seleccionar una asignatura a inscribir.", "error");
            }

        };

        $scope.eliminarInscripcion = function (idAsignatura) {
            var inscripcion = {idEstudiante: $scope.estudiante.id, idAsignatura: idAsignatura};
            console.log(inscripcion);
            $http.post(BASE + '/resources/inscripcion/eliminarInscripcion', inscripcion)
                    .success(function (data) {
                        $scope.consultarAsignaturasInscritas();
                        $scope.consultarAsignaturasPorInscribir();
                        sweetAlert("Correcto", "Inscripción eliminada con éxito.", "success");
                    })
                    .error(function (data) {
                        sweetAlert("Error", "No fue posible eliminar la inscripción, por favor intente de nuevo.", "error");
                    });
        };

        $scope.consultarAsignaturasPorInscribir = function () {
            $http.get(BASE + '/resources/inscripcion/obtenerAsignaturasPorInscribir/' + $scope.estudiante.id).
                    then(function (response) {
                        $scope.asignaturasPorInscribir = response.data;
                    });
        };
    }]);

'use strict';

var app = angular.module('GestionEstudiantesApp', [
    'GestionEstudiantesApp.Asignaturas',
    'GestionEstudiantesApp.Estudiantes',
    'GestionEstudiantesApp.Facultades',
    'GestionEstudiantesApp.Inscripciones'
]);

app.config(function ($routeProvider, $httpProvider) {
    $routeProvider.when('/consultar_estudiantes', {templateUrl: 'views/estudiantes/consultar_estudiantes.html', controller: 'EstudianteCtrl'});
    $routeProvider.when('/editar_estudiante/:id', {templateUrl: 'views/estudiantes/editar_estudiante.html', controller: 'EstudianteCtrl'});
    $routeProvider.when('/crear_estudiante', {templateUrl: 'views/estudiantes/crear_estudiante.html', controller: 'EstudianteCtrl'});

    $routeProvider.when('/consultar_asignaturas', {templateUrl: 'views/asignaturas/consultar_asignaturas.html', controller: 'AsignaturaCtrl'});
    $routeProvider.when('/editar_asignatura/:id', {templateUrl: 'views/asignaturas/editar_asignatura.html', controller: 'AsignaturaCtrl'});
    $routeProvider.when('/crear_asignatura', {templateUrl: 'views/asignaturas/crear_asignatura.html', controller: 'AsignaturaCtrl'});

    $routeProvider.when('/consultar_facultades', {templateUrl: 'views/facultades/consultar_facultades.html', controller: 'FacultadCtrl'});
    $routeProvider.when('/editar_facultad/:id', {templateUrl: 'views/facultades/editar_facultad.html', controller: 'FacultadCtrl'});
    $routeProvider.when('/crear_facultad', {templateUrl: 'views/facultades/crear_facultad.html', controller: 'FacultadCtrl'});

    $routeProvider.when('/gestionar_inscripcion', {templateUrl: 'views/inscripciones/gestionar_inscripcion.html', controller: 'InscripcionCtrl'});

    $routeProvider.otherwise({redirectTo: '/consultar_estudiantes'});

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

})

.filter('startFrom', function(){
	return function(data, start) {
		if (!data || !data.length) { return; }
		return data.slice(start);
	}
});

app.constant('BASE', 'http://localhost:8080/gestion-estudiantes-backend');
package co.edu.javeriana.gestionestudiantes.servicios;

import co.edu.javeriana.gestionestudiantes.entidades.Facultad;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("facultad")
@ApplicationPath("resources")
public class FacultadServices extends Application {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @GET
    @Path("/obtenerFacultad/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Facultad obtenerFacultad(@PathParam("id") Long id) {
        return entityManager.find(Facultad.class, id);
    }
    
    @GET
    @Path("/obtenerTodasFacultades")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Facultad> obtenerTodasFacultades() {
        Query query = entityManager.createQuery("SELECT f FROM Facultad f ORDER BY f.codigo");
        return query.getResultList();
    }
    
    @POST
    @Path("/agregarFacultad")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Facultad agregarFacultad(Facultad facultad) {
        Facultad facultadCrear = new Facultad();
        facultadCrear.setCodigo(facultad.getCodigo());
        facultadCrear.setNombre(facultad.getNombre());
        facultadCrear.setDescripcion(facultad.getDescripcion());
        entityManager.persist(facultadCrear);
        return facultadCrear;
    }
    
    @POST
    @Path("/actualizarFacultad")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Facultad actualizarFacultad(Facultad facultad) {
        Facultad facultadActualizar = obtenerFacultad(facultad.getId());
        facultadActualizar.setCodigo(facultad.getCodigo());
        facultadActualizar.setNombre(facultad.getNombre());
        facultadActualizar.setDescripcion(facultad.getDescripcion());
        facultadActualizar = entityManager.merge(facultadActualizar);
        return facultadActualizar;
    }
    
    @POST
    @Path("/eliminarFacultad")
    @Consumes(MediaType.APPLICATION_JSON)
    public void eliminarFacultad(Long id) {
        entityManager.remove(obtenerFacultad(id));
    }
    
}

package co.edu.javeriana.gestionestudiantes.servicios;

import co.edu.javeriana.gestionestudiantes.entidades.Estudiante;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("estudiante")
@ApplicationPath("resources")
public class EstudianteServices extends Application {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @GET
    @Path("/obtenerEstudiante/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estudiante obtenerEstudiante(@PathParam("id") Long id) {
        return entityManager.find(Estudiante.class, id);
    }
    
    @GET
    @Path("/obtenerTodosEstudiantes")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Estudiante> obtenerTodosEstudiantes() {
        Query query = entityManager.createQuery("SELECT e FROM Estudiante e ORDER BY e.id");
        return query.getResultList();
    }
    
    @POST
    @Path("/agregarEstudiante")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Estudiante agregarEstudiante(Estudiante estudiante) {
        Estudiante estudianteCrear = new Estudiante();
        estudianteCrear.setNumeroDocumento(estudiante.getNumeroDocumento());
        estudianteCrear.setNombres(estudiante.getNombres());
        estudianteCrear.setApellidos(estudiante.getApellidos());
        estudianteCrear.setDireccion(estudiante.getDireccion());
        estudianteCrear.setTelefono(estudiante.getTelefono());
        entityManager.persist(estudianteCrear);
        return estudianteCrear;

    }
    
    @POST
    @Path("/actualizarEstudiante")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Estudiante actualizarEstudiante(Estudiante estudiante) {
        Estudiante estudianteActualizar = obtenerEstudiante(estudiante.getId());
        estudianteActualizar.setNumeroDocumento(estudiante.getNumeroDocumento());
        estudianteActualizar.setNombres(estudiante.getNombres());
        estudianteActualizar.setApellidos(estudiante.getApellidos());
        estudianteActualizar.setDireccion(estudiante.getDireccion());
        estudianteActualizar.setTelefono(estudiante.getTelefono());
        estudianteActualizar = entityManager.merge(estudianteActualizar);
        return estudianteActualizar;
    }
    
    @POST
    @Path("/eliminarEstudiante")
    @Consumes(MediaType.APPLICATION_JSON)
    public void eliminarEstudiante(Long id) {
        entityManager.remove(obtenerEstudiante(id));
    }
    
}

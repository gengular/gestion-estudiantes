package co.edu.javeriana.gestionestudiantes.entidades;

import java.io.Serializable;
import javax.validation.constraints.NotNull;

public class Inscripcion implements Serializable {
    
    @NotNull
    private Long idEstudiante;
    @NotNull
    private Long idAsignatura;

    public Long getIdEstudiante() {
        return idEstudiante;
    }

    public void setIdEstudiante(Long idEstudiante) {
        this.idEstudiante = idEstudiante;
    }

    public Long getIdAsignatura() {
        return idAsignatura;
    }

    public void setIdAsignatura(Long idAsignatura) {
        this.idAsignatura = idAsignatura;
    }
    
}

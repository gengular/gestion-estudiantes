package co.edu.javeriana.gestionestudiantes.servicios;

import co.edu.javeriana.gestionestudiantes.entidades.Asignatura;
import co.edu.javeriana.gestionestudiantes.entidades.Estudiante;
import co.edu.javeriana.gestionestudiantes.entidades.Inscripcion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("inscripcion")
@ApplicationPath("resources")
public class InscripcionServices extends Application {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @GET
    @Path("/obtenerAsignaturasInscritas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    
    public List<Asignatura> obtenerAsignaturasInscritas(@PathParam("id") Long id) {
        Query query = entityManager.createQuery("SELECT a FROM Estudiante e JOIN e.asignaturas a WHERE e.id = :id")
                .setParameter("id", id);
        return query.getResultList();
    }
    
    @GET
    @Path("/obtenerAsignaturasPorInscribir/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> obtenerAsignaturasPorInscribir(@PathParam("id") Long id) {
        Query query = entityManager.createNativeQuery("SELECT ID, CODIGO, NOMBRE, DESCRIPCION, SEMESTRE, ESTADO, FACULTAD_ID FROM ASIGNATURA WHERE ID NOT IN (SELECT ASIGNATURA_ID FROM ESTUDIANTE_ASIGNATURA WHERE ESTUDIANTE_ID = ?1)", Asignatura.class);
        query.setParameter(1, id);
        return (List<Asignatura>)query.getResultList();
    }
    
    @POST
    @Path("/inscribirAsignatura")
    @Consumes(MediaType.APPLICATION_JSON)
    public void inscribirAsignatura(Inscripcion inscripcion) {
        entityManager.createNativeQuery("INSERT INTO ESTUDIANTE_ASIGNATURA(ESTUDIANTE_ID, ASIGNATURA_ID) VALUES(?1, ?2)")
            .setParameter(1,inscripcion.getIdEstudiante())
            .setParameter(2,inscripcion.getIdAsignatura())   
            .executeUpdate();
    }
    
    @POST
    @Path("/eliminarInscripcion")
    @Consumes(MediaType.APPLICATION_JSON)
    public void eliminarInscripcion(Inscripcion inscripcion) {
        entityManager.createNativeQuery("DELETE FROM ESTUDIANTE_ASIGNATURA WHERE ESTUDIANTE_ID = ?1 AND ASIGNATURA_ID = ?2")
            .setParameter(1,inscripcion.getIdEstudiante())
            .setParameter(2,inscripcion.getIdAsignatura())   
            .executeUpdate();
    }
    
    @GET
    @Path("/obtenerEstudiante/{numeroDocumento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estudiante obtenerEstudiante(@PathParam("numeroDocumento") String numeroDocumento) {
        Query query = entityManager.createQuery("SELECT e FROM Estudiante e WHERE e.numeroDocumento = :numeroDocumento");
        query.setParameter("numeroDocumento", numeroDocumento);
        return (Estudiante)query.getSingleResult();
    }
    
    
    
}

package co.edu.javeriana.gestionestudiantes.servicios;

import co.edu.javeriana.gestionestudiantes.entidades.Asignatura;
import co.edu.javeriana.gestionestudiantes.entidades.Facultad;

import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("asignatura")
@ApplicationPath("resources")

public class AsignaturaServices extends Application {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @GET
    @Path("/obtenerAsignatura/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Asignatura obtenerAsignatura(@PathParam("id") Long id) {
        return entityManager.find(Asignatura.class, id);
    }
    
    @GET
    @Path("/obtenerTodasAsignaturas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> obtenerTodasAsignatura() {
        Query query = entityManager.createQuery("SELECT a FROM Asignatura a ORDER BY a.codigo");
        return query.getResultList();
    }
    
    @POST
    @Path("/agregarAsignatura")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Asignatura agregarAsignatura(Asignatura asignatura) {
        Asignatura asignaturaCrear = new Asignatura();
        asignaturaCrear.setCodigo(asignatura.getCodigo());
        asignaturaCrear.setNombre(asignatura.getNombre());
        asignaturaCrear.setDescripcion(asignatura.getDescripcion());
        asignaturaCrear.setSemestre(asignatura.getSemestre());
        asignaturaCrear.setEstado(asignatura.getEstado());
        asignaturaCrear.setFacultad(entityManager.find(Facultad.class, asignatura.getFacultad().getId()));
        entityManager.persist(asignaturaCrear);
        return asignaturaCrear;
    }
    
    @POST
    @Path("/actualizarAsignatura")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Asignatura actualizarAsignatura(Asignatura asignatura) {
        Asignatura asignaturaActualizar = obtenerAsignatura(asignatura.getId());
        asignaturaActualizar.setCodigo(asignatura.getCodigo());
        asignaturaActualizar.setNombre(asignatura.getNombre());
        asignaturaActualizar.setDescripcion(asignatura.getDescripcion());
        asignaturaActualizar.setSemestre(asignatura.getSemestre());
        asignaturaActualizar.setEstado(asignatura.getEstado());
        asignaturaActualizar.setFacultad(asignatura.getFacultad());
        asignaturaActualizar = entityManager.merge(asignaturaActualizar);
        return asignaturaActualizar;
    }
    
    @POST
    @Path("/eliminarAsignatura")
    @Consumes(MediaType.APPLICATION_JSON)
    public void eliminarAsignatura(Long id) {
        entityManager.remove(obtenerAsignatura(id));
    }
    
}
